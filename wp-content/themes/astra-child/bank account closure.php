<?php
/**
 * Template Name: Bank Account Closure
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Astra
 * @since 1.0.0
 */

    get_header();
?>
    
  <link rel="stylesheet" href="https://kifaayat.com/wp-content/themes/astra-child/bac.css">
  <link rel="stylesheet" href="https://kifaayat.com/wp-content/themes/astra-child/custom-select.css">

<!--<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <title>Bank Account Closure | Kifaayat.com</title>
  <link rel="stylesheet" href="https://kifaayat.com/wp-content/themes/astra-child/bac.css">
  <link rel="stylesheet" href="https://kifaayat.com/wp-content/themes/astra-child/custom-select.css">
</head>
<body>-->
    
    <section style="background: #000;">
		<div class="container-row">

			<div class="formBox">
				<!-- partial:index.partial.html -->
				<!-- multistep form -->
				<form id="msform">
					<!-- progressbar -->
					<!-- <ul id="progressbar">
						<li class="active"></li>
						<li></li>
						<li></li>
						<li></li>
						<li></li>
						<li></li>
						<li></li>
						<li></li>
						<li></li>
						<li></li>
					</ul> -->
					<!-- fieldsets -->
					<fieldset>
						<!-- <h2 class="fs-title">Select your Bank</h2> -->
						<h3 class="fs-subtitle">Select the bank for which you wanna close account.</h3>
					    <!-- <p class="help-block">Select your your Bank</p> -->
					    <div  class="style1">
						    <select class="form-control bankSelection" name="CAT_Custom_1" id="CAT_Custom_1"  onkeydown="if(this.value.length>=4000)this.value=this.value.substring(0,3999);">
						    	<option value="" disabled selected>-- Select your Bank --</option>
						    	<option value="AXIS" >Axis Bank</option>
						    	<option value="Bandhan">Bandhan Bank</option>
						    	<option value="BOI" >Bank of India</option>
						    	<option value="BOB" >Bank of Baroda</option>
						    	<option value="Canara" >Canara Bank</option>
						    	<option value="CBI" >Central Bank of India</option>
						    	<option value="Citi" >Citi Bank</option>
						    	<option value="DBS">DBS Bank</option>
						    	<option value="HDFC">HDFC Bank</option>
						    	<option value="ICICI">ICICI Bank</option>
						    	<option value="IDBI">IDBI Bank</option>
						    	<option value="Indian">Indian Bank</option>
						    	<option value="IOB">Indian Overseas Bank</option>
						    	<option value="IndusInd">IndusInd Bank</option>
						    	<option value="Kotak">Kotak Bank</option>
						    	<option value="PNB">Punjab National Bank</option>
						    	<option value="RBL">RBL Bank</option>
						    	<option value="SBI">SBI Bank</option>
						    	<option value="SCB">Standard Chartered Bank</option>
						    	<option value="UCO">UCO Bank</option>
						    	<option value="Union">Union Bank</option>
						    	<option value="Yes">Yes Bank</option>
						    </select>
						</div>
						<input type="button" name="next" class="next action-button" id="selectBank_next" style="display:none;" value="&rarr;" />
					</fieldset>
					<!-- <fieldset>
						<h3 class="fs-subtitle">Do you wan to activate or close the Bank Account?</h3>
						
						<p class="radio">
							<input type="radio" class="form-control" name="CAT_Custom_2" id="CAT_Custom_2"  onkeydown="if(this.value.length>=4000)this.value=this.value.substring(0,3999);">
							<label for="CAT_Custom_2">Activate</label>
						</p>
						<p class="radio">
							<input type="radio" class="form-control" name="CAT_Custom_3" id="CAT_Custom_3"  onkeydown="if(this.value.length>=4000)this.value=this.value.substring(0,3999);">
							<label for="CAT_Custom_3">Close</label>
						</p>
						<input type="button" name="previous" class="previous action-button" value="&larr;" />
						<input type="button" name="next" class="next action-button" value="&rarr;" />
					</fieldset>
					<fieldset>
						<h3 class="fs-subtitle">Title</h3>
						<input type="text" class="form-control" placeholder="Enter Email ID" name="CAT_Custom_4" id="CAT_Custom_4" onkeydown="if(this.value.length>=4000)this.value=this.value.substring(0,3999);">
						<input type="button" name="previous" class="previous action-button" value="&larr;" />
						<input type="button" name="next" class="next action-button" value="&rarr;" />
					</fieldset>
					<fieldset>
						<h3 class="fs-subtitle">Title</h3>
						<textarea class="form-control" name="CAT_Custom_5" id="CAT_Custom_5" rows="4" onkeydown="if(this.value.length>=4000)this.value=this.value.substring(0,3999);"></textarea>
						<input type="button" name="previous" class="previous action-button" value="&larr;" />
						<input type="submit" name="submit" class="submit action-button" value="&rarr;" />
					</fieldset> -->
				</form>
			</div>
			<div class="previewBox" id="solutionPreviewDiv">
				<!-- Will show solution here -->
				<iframe id="frame" src="" width="100%" height="100%"></iframe>
			</div>
		</div>
	</section>

<?php get_footer(); ?>

<!-- jQuery -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js" type="text/javascript"></script>
<!-- partial -->
<script type="text/javascript">
	var banksData;
	var processData;
	var next;
	var previous;
	var current;
	jQuery(document).ready(function(){
		function findObjectInArrayByProperty(array, propertyName, propertyValue) {
			return array.find((o) => { return o[propertyName] === propertyValue });
		}
        $.getJSON('https://kifaayat.com/wp-content/themes/astra-child/data.json',function(data){
            console.log('success', data);
			banksData = data
        }).error(function(err){
            console.log('error', err);
		});
		function renderField (step) {
			console.log('Rendering Field... for step', step)
			// Add field to the form according to the properties of the process step
			var question = $('<h3>', { class: 'fs-subtitle' })
			question.text(step.question)
			var divForSelect = $('<div>', { class: 'style1' })
			var select = $('<select>', { class: 'form-control renderedField' })
			select.append($('<option/>')
					.attr("disabled", "")
					.attr("selected", "")
					.text("--")
				)
			var newFieldSet = $('<fieldset>')
			var optionsInData = step.options
			$.each(optionsInData, function (index, item) {
				console.log('Options : ', index, ' : ', item)
				select.append($('<option/>')
					.attr("value",  item.value)
					.text(item.value)
				)
			})
			divForSelect.append(select)
			newFieldSet.append(question)
			newFieldSet.append(divForSelect)
			newFieldSet.append($('<input/>')
					.attr("type", "button")
					.attr("name", "previous")
					.attr("class", "previous action-button")
					.attr("value", "←"))
			newFieldSet.append($('<input/>')
					.attr("id", step.id+"_next")
					.attr("type", "button")
					.attr("name", "next")
					.attr("class", "next action-button")
					.attr("value", "→")
					.attr("style", "display:none;"))
			console.log("Render this field: ", newFieldSet)
			$("#msform").append(newFieldSet)
			current = step.id
		}
		$(document).on('change', '.renderedField', function () {
			var value = $(this).val()
			var currentStep = findObjectInArrayByProperty(processData, "id", current)
			var selectedOption = findObjectInArrayByProperty(currentStep.options, "value", value)
			console.log("Slected option :", selectedOption)
			if (selectedOption.actionType == "question") {
				//Render next step field 
				console.log("next action : ", selectedOption.action)
				var nextField = findObjectInArrayByProperty(processData, "id", selectedOption.action)
				if (nextField) {
					//Show next button
					var nextBtnId = "#"+current+"_next"
					console.log("next button id : ",  nextBtnId)
					$(nextBtnId).show()
					renderField(nextField)
				}
			} else if (selectedOption.actionType == "link") {
				//Render html inside solution
				$("#frame").attr("src", selectedOption.action);
				if (selectedOption.continue) {
					//Render next step field 
					console.log("next action : ", selectedOption.continue)
					var nextField = findObjectInArrayByProperty(processData, "id", selectedOption.continue)
					if (nextField) {
						//Show next button
						var nextBtnId = "#"+current+"_next"
						console.log("next button id : ",  nextBtnId)
						$(nextBtnId).show()
						renderField(nextField)
					}
				}
			}
		})
		var current_fs, next_fs, previous_fs // fieldsets
		var left, opacity, scale // fieldset properties which we will animate
		var animating; // flag to prevent quick multi-click glitches
		$(document).on('click', '.next', function () {
			console.log('Next Clicked...')
			if (animating) return false
			animating = true

			current_fs = $(this).parent()
			next_fs = $(this).parent().next()

			// activate next step on progressbar using the index of next_fs
			$('#progressbar li').eq($('fieldset').index(next_fs)).addClass('active')

			// show the next fieldset
			next_fs.show()
			// hide the current fieldset with style
			current_fs.animate({ opacity: 0 }, {
				step: function (now, mx) {
				// as the opacity of current_fs reduces to 0 - stored in "now"
				// 1. scale current_fs down to 80%
				scale = 1 - (1 - now) * 0.2
				// 2. bring next_fs from the right(50%)
				left = (now * 50) + '%'
				// 3. increase opacity of next_fs to 1 as it moves in
				opacity = 1 - now
				current_fs.css({ 'transform': 'scale(' + scale + ')' })
				next_fs.css({ 'left': left, 'opacity': opacity })
				},
				duration: 500,
				complete: function () {
				current_fs.hide()
				animating = false
				},
				// this comes from the custom easing plugin
				easing: 'easeOutQuint'
			})
		})
		$(document).on('click', '.previous', function () {
			if (animating) return false
			animating = true

			current_fs = $(this).parent()
			previous_fs = $(this).parent().prev()

			// de-activate current step on progressbar
			$('#progressbar li').eq($('fieldset').index(current_fs)).removeClass('active')

			// show the previous fieldset
			previous_fs.show()
			// hide the current fieldset with style
			current_fs.animate({opacity: 0}, {
				step: function (now, mx) {
				// as the opacity of current_fs reduces to 0 - stored in "now"
				// 1. scale previous_fs from 80% to 100%
				scale = 0.8 + (1 - now) * 0.2
				// 2. take current_fs to the right(50%) - from 0%
				left = ((1 - now) * 50) + '%'
				// 3. increase opacity of previous_fs to 1 as it moves in
				opacity = 1 - now
				current_fs.css({'left': left})
				previous_fs.css({'transform': 'scale(' + scale + ')', 'opacity': opacity})
				},
				duration: 500,
				complete: function () {
				current_fs.hide()
				animating = false
				},
				// this comes from the custom easing plugin
				easing: 'easeOutQuint'
			})
		})
		$('.bankSelection').selectCF({
			change: function () {
				var value = $(this).val()
				var text = $(this).children('option:selected').html()
				console.log(value + ' : ' + text)
				console.log('the banks Data : ', banksData)
				processData = banksData[value]
				console.log('process Data : ', processData)
				var firstProcess = findObjectInArrayByProperty(processData, "first", true);
				console.log('get fist process from Data : ', firstProcess)
				renderField(firstProcess)
				$("#selectBank_next").show()
			}
		})
	});	
</script>
<script  src="https://kifaayat.com/wp-content/themes/astra-child/bac.js"></script>
<script src="https://kifaayat.com/wp-content/themes/astra-child/custom-select.js" type="text/javascript"></script>
<script  src="https://kifaayat.com/wp-content/themes/astra-child/renderer.js"></script>